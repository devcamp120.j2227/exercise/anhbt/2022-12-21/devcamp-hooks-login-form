import { Button, Grid, TextField, Typography } from '@mui/material'
import React, { useState } from 'react'

function SignUp() {
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const handleSignUp = () => { }
  return (
    <Grid container mt={3} sx={{ alignContent: 'center', justifyContent: 'center', flexDirection: 'column', textAlign:"center" }}>
      <Grid item >
        <Typography variant="h3" component='div' sx={{m: 2}}>
          Sign up for free
        </Typography>
      </Grid>

      <Grid item sx={{m: 1}}>
        <TextField sx={{mx: 1}} id='inp-firstname' label='First Name' variant='outlined' onChange={(e) => setFirstname(e.target.value)} value={firstname} />
        <TextField id='inp-lastname' label='Last Name' variant='outlined' onChange={(e) => setLastname(e.target.value)} value={lastname} />
      </Grid>
      <Grid item sx={{m: 1}}>
        <TextField id='inp-username' label='Email' variant='outlined' onChange={(e) => setUsername(e.target.value)} value={username} />
      </Grid>
      <Grid item sx={{m: 1}}>
        <TextField id='inp-password' label='Password' variant='outlined' onChange={(e) => setPassword(e.target.value)} value={password} />
      </Grid>
      <Grid sx={{m: 2}}>
        <Button id='btn-sign-up' variant='contained' onClick={(e) => handleSignUp(e)}>SIGN UP</Button>
      </Grid>
    </Grid>
  )
}

export default SignUp