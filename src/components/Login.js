import { Button, Grid, TextField, Typography } from '@mui/material'
import React, { useState } from 'react'

function Login() {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const style = { alignContent: 'center', justifyContent: 'center', flexDirection: 'column' , textAlign: 'center'}
  const handleSignUp = () => { }
  return (
    <Grid container mt={3} sx={style}>
      <Grid item >
        <Typography variant="h3" component='div' sx={{m: 2}}>
          Welcome Back!
        </Typography>
      </Grid>
      <Grid item sx={{m: 1}}>
        <TextField id='inp-username' label='Email' variant='outlined' onChange={(e) => setUsername(e.target.value)} value={username} />
      </Grid>
      <Grid item sx={{m: 1}}>
        <TextField id='inp-password' label='Password' variant='outlined' onChange={(e) => setPassword(e.target.value)} value={password} />
      </Grid>
      <Grid sx={{m: 2}}>
        <Button id='btn-login' variant='contained' onClick={(e) => handleSignUp(e)}>LOGIN</Button>
      </Grid>
    </Grid>
  )
}

export default Login