import { Container, Button, ButtonGroup, Grid } from '@mui/material'
import { useState } from 'react';
import Login from './components/Login';
import SignUp from './components/SignUp';

const style = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center'
}

function App() {
  const [isLogin, setIsLogin] = useState(true)
  return (
    <Container sx={{ height: '300px' }}>
      <Grid container sx={style} mt={5}>
        <ButtonGroup variant="contained" mt={5} sx={style}>
          <Button color={isLogin ? 'inherit' : "success"} onClick={() => setIsLogin(false)}>SIGN UP</Button>
          <Button color={isLogin ? 'success' : "inherit"} onClick={() => setIsLogin(true)}>LOGIN</Button>
        </ButtonGroup>
        {
          isLogin ? <Login /> : <SignUp />
        }
      </Grid>
    </Container>
  );
}

export default App;
